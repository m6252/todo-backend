package main

import (
	"os"

	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
	"go.mongodb.org/mongo-driver/bson"
)

func ApiHandlers(app *fiber.App) *fiber.App {
	app.Get("/", GetHandler)
	app.Post("/", PostHandler)

	return app
}
func GetHandler(c *fiber.Ctx) error {
	mode := os.Getenv("Mode")
	var todos *TodoList
	var err error
	if mode == "Test" {
		todos, err = MockGet()
	} else {
		todos, err = Get()
	}
	c.JSON(todos)
	c.Status(fiber.StatusOK)
	return err

}

func MockGet() (*TodoList, error) {
	faketodo1 := Model{ID: "1", Todo: "buy meat", V: 0}
	faketodo2 := Model{ID: "2", Todo: "buy milk", V: 0}
	faketodo3 := Model{ID: "3", Todo: "clear house", V: 0}
	todoList := TodoList{
		Todos: []Model{faketodo1, faketodo2, faketodo3},
	}
	return &todoList, nil
}
func Get() (*TodoList, error) {
	client, ctx := MongoClient()
	database := client.Database("todo").Collection("todos")
	todoList := TodoList{}
	cursor, err := database.Find(ctx, bson.M{})
	if err != nil {
		return nil, err
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		model := Model{}
		cursor.Decode(&model)
		todoList.Todos = append(todoList.Todos, model)
	}
	defer client.Disconnect(ctx)
	return &todoList, nil
}
func AddTodoList(m *Model) error {
	mode := os.Getenv("Mode")
	if m.Todo == "" {
		return fiber.NewError(404, "You should fill textbox for add todo")
	}

	if m.ID == "" {
		m.ID = uuid.New().String()
	}
	client, ctx := MongoClient()
	if mode == "Test" {
		db := client.Database("todo").Collection("todostest")
		db.InsertOne(ctx, m)
	} else {
		db := client.Database("todo").Collection("todos")
		db.InsertOne(ctx, m)
	}
	defer client.Disconnect(ctx)
	return nil
}

func PostHandler(c *fiber.Ctx) error {
	mode := os.Getenv("Mode")

	model := Model{}
	errA := c.BodyParser(&model)
	if errA != nil {
		return errA
	}
	var data *Model
	var errB error

	if mode == "Test" {
		c.Status(fiber.StatusCreated)
	} else {
		errB = AddTodoList(&model)
	}
	c.JSON(data)
	c.Status(fiber.StatusCreated)
	return errB
}
