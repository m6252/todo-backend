# Todo App Backend 

* [Production Backend](http://35.247.59.150) todo app
* [Test Backend](http://34.79.241.123) todo app

## Libraries & Frameworks:

* [Go](https://go.dev/) for download golang
* [Pact](https://github.com/pact-foundation/pact-go) for CDC tests
* [Fiber](https://github.com/gofiber/fiber/v2) for api
* [MongoDB Atlas](https://www.mongodb.com/) register and get your free cluster
* [google/uuid](https://github.com/google/uuid) for get go codes import

## Setup & Run
- Run `go mod download` for package installations.
- Run `go test -run TestUnit -v` for unit tests.
- Run `go test -run TestAcceptance  -v` for acceptance backend tests.
- Run `Mode=Test go test -run TestProvider -v` for cdc provider tests.
- Run `go build .` and then `./backend` for run service.

