package main

import (
	"testing"

	"github.com/gofiber/fiber/v2"
	. "github.com/smartystreets/goconvey/convey"
)

//hi
func TestUnit_ApiHandlers(t *testing.T) {
	Convey("Render a apihandlers function", t, func() {
		app := fiber.New()

		So(ApiHandlers(app), ShouldEqual, app)
	})
}

// func TestUnit_DataBase(t *testing.T) {
// 	Convey("Is database connected", t, func() {
// 		client, ctx := MongoClient()
// 		So(ctx, ShouldNotBeNil)
// 		So(client, ShouldNotBeNil)

// 	})
// }
