package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"testing"

	"github.com/gofiber/fiber/v2"
	. "github.com/smartystreets/goconvey/convey"
)

func TestAcceptance_TodosGet(t *testing.T) {
	Convey("Given Todo models", t, func() {
		faketodo1 := Model{ID: "1", Todo: "buy meat", V: 0}
		faketodo2 := Model{ID: "2", Todo: "buy milk", V: 0}
		faketodo3 := Model{ID: "3", Todo: "clear house", V: 0}

		AddTodoList(&faketodo1)
		AddTodoList(&faketodo2)
		AddTodoList(&faketodo3)

		Convey("When GET request to index page", func() {
			request, _ := http.NewRequest(http.MethodGet, "/", nil)
			request.Header.Add("Content-Type", "application/json")
			app := fiber.New()
			app = ApiHandlers(app)

			response, err := app.Test(request, 20000)
			So(err, ShouldBeNil)

			Convey("Then must return status 200", func() {
				So(response.StatusCode, ShouldEqual, fiber.StatusOK)
				Convey("Then must return todo items", func() {
					responseBody, err := ioutil.ReadAll(response.Body)
					So(err, ShouldBeNil)

					tododata := TodoList{}

					err = json.Unmarshal(responseBody, &tododata)
					So(err, ShouldBeNil)
					fmt.Print(tododata.Todos[0])
					So(tododata.Todos[0].ID, ShouldEqual, faketodo1.ID)
					So(tododata.Todos[0].Todo, ShouldEqual, faketodo1.Todo)
					So(tododata.Todos[1].ID, ShouldEqual, faketodo2.ID)
					So(tododata.Todos[1].Todo, ShouldEqual, faketodo2.Todo)
					So(tododata.Todos[2].ID, ShouldEqual, faketodo3.ID)
					So(tododata.Todos[2].Todo, ShouldEqual, faketodo3.Todo)

				})
			})
		})

	})
}
func TestAcceptance_TodoPost(t *testing.T) {
	Convey("Given Post data", t, func() {
		faketodo := Model{ID: "0", Todo: "buy cola"}
		Convey("When post request to /", func() {
			todoByte, err := json.Marshal(faketodo)
			So(err, ShouldBeNil)
			todoReader := bytes.NewReader(todoByte)
			request, _ := http.NewRequest(http.MethodPost, "/", todoReader)
			request.Header.Add("Content-Type", "application/json")
			app := fiber.New()
			app = ApiHandlers(app)

			response, err := app.Test(request, 20000)
			So(err, ShouldBeNil)
			Convey("Then i should see status code created", func() {
				So(response.StatusCode, ShouldEqual, fiber.StatusCreated)
			})
		})
	})

}
