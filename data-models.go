package main

type Model struct {
	ID   string `json:"_id" bson:"_id"`
	Todo string `json:"todo" bson:"todo"`
	V    int    `json:"_v" bson:"_v"`
}

type TodoList struct {
	Todos []Model `json:"todos"`
}
