package main

import (
	"testing"

	"github.com/gofiber/fiber/v2"
	"github.com/pact-foundation/pact-go/dsl"
	"github.com/pact-foundation/pact-go/types"
)

func TestProvider_Pact(t *testing.T) {
	pact := &dsl.Pact{
		Provider: "todo-app-backend",
	}
	pact.VerifyProvider(t, types.VerifyRequest{
		ProviderBaseURL:            "http://127.0.0.1:8081",
		BrokerURL:                  "https://ehilmidag.pactflow.io",
		BrokerToken:                "tXCE0GHKuVvglcFKTJChGw",
		PublishVerificationResults: true,
		ProviderVersion:            "todo-app-backend-v1",

		BeforeEach: func() error {
			app := fiber.New()
			app = ApiHandlers(app)
			go app.Listen(":8081")
			return nil
		},
		StateHandlers: types.StateHandlers{
			"All todo items": func() error {
				return nil
			},
			"Post a todo": func() error {
				return nil
			},
		},
	})
}
