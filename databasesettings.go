package main

import (
	"context"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func MongoClient() (*mongo.Client, context.Context) {
	databaseUrl := "mongodb+srv://ehilmi:ehd123ehd@holy.gmtxs.mongodb.net/myFirstDatabase?retryWrites=true&w=majority"

	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	clientOptions := options.Client().ApplyURI(databaseUrl)

	client, _ := mongo.Connect(ctx, clientOptions)

	return client, ctx
}
